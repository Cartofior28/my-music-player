<?php
if(isset($_POST['loginButton'])) {
	//Loggin button was pressed
}

if(isset($_POST['registerButton'])) {
	//Register button was pressed
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Welcome to MyPlayer!</title>
</head>
<body>

	<form id="loginForm" action="register.php" method="POST">
		<h2>Log in to your account</h2>
		<p>
			<label for="loginUsername">Username</label>
			<input id="loginUsername" type="text" name="loginUsername" placeholder="Username">
		</p>

		<p>
			<label for="loginPassword">Password</label>
			<input id="loginPassword" type="password" name="loginPassword" placeholder="Password">
		</p>

		<button type="submit" name="loginButton">LOG IN</button>
	</form>

	<form id="registerForm" action="register.php" method="POST">
		<h2>Create new account</h2>
		<p>
			<label for="username">Username</label>
			<input id="username" type="text" name="username" placeholder="e.g. razvanMiu">
		</p>
		<p>
			<label for="firstName">First Name</label>
			<input id="firstName" type="text" name="firstName" placeholder="e.g. Razvan">
		</p>
		<p>
			<label for="lastName">Last Name</label>
			<input id="lastName" type="text" name="lastName" placeholder="e.g. Miu">
		</p>
		<p>
			<label for="email">Email</label>
			<input id="email" type="email" name="email" placeholder="e.g. razvan.miu@yahoo.com">
		</p>
		<p>
			<label for="email2">Confirm email</label>
			<input id="email2" type="email" name="email2" placeholder="e.g. razvan.miu@yahoo.com">
		</p>
		<p>
			<label for="password">Password</label>
			<input id="password" type="password" name="password" placeholder="e.g. 12345">
		</p>
		<p>
			<label for="password2">Confirm password</label>
			<input id="password2" type="password" name="password2" placeholder="e.g. 12345">
		</p>

		<button type="submit" name="registerButton">SIGN UP</button>
	</form>
</body>
</html>